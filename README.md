# SS004.K22.BaiTap.2.7

Bài tập nhóm môn Kỹ năng nghề nghiệp SS004.K22:
+ Tập sử dụng gitlab
+ Code chung bài game con Rắn

Gồm các thành viên:
+ Bùi Cao Doanh - MSSV: 19521366
+ Bùi Trần Ngọc Dũng - MSSV: 19521385
+ Nguyễn Thành Luân - MSSV: 19521809

Nhiệm vụ cụ thể:
+ Thực hiện soạn báo cáo bằng Latex - Convert sang PDF: Bùi Trần Ngọc Dũng
+ Thực hiện lập trình trò chơi: Bùi Cao Doanh (code chính) , Nguyễn Thành Luân (chỉnh sửa lỗi)
+ Thực hiện phân công công việc Trello: Nguyễn Thành Luân

Nội dung source code:
*Header files: 
+ file FunctionGame.h chứa các hàm xử lý hiển thị rắn, hiển thị mồi, sự kiện rắn ăn mồi, vẽ tường.
+ file snake_console.h: các hàm xử lý màn hình

*Source files:
+ file snake_console.cpp
+ file MainGame.cpp: file main chạy các lệnh ở FunctionGame.h

Một số ảnh game:
![Alt-Text](/demo/demo1.png)
![Alt-Text](/demo/demo2.png)
![Alt-Text](/demo/demo3.png)

GV hướng dẫn: Nguyễn Văn Toàn
