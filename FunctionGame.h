#define MAX 100
#define LEN 1
#define XUONG 2
#define TRAI 3
#define PHAI 4
#define TUONG_TREN 0
#define TUONG_DUOI 13
#define TUONG_TRAI 1
#define TUONG_PHAI 70
#include "snake_console.h"
#include <iostream>
#include <time.h>
using namespace std;
struct toado
{
	int x;
	int y;
};

toado ran[MAX];
int soDot = 3;

void Nocursortype() //Khong hien dau nhap nhay
{
	CONSOLE_CURSOR_INFO Info;
	Info.bVisible = FALSE;
	Info.dwSize = 20;
	SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE), &Info);
}

void khoiTaoRan() //Ham khoi tao ran
{
	ran[0].x = 4;
	ran[1].x = 5;
	ran[2].x = 6;
	ran[0].y = ran[1].y = ran[2].y = 5;
}

void hienthiRan(toado dotcuoicu) //Ham hien thi ran
{
	for (int i = 0; i < soDot; i++)
	{
		gotoXY(ran[i].x, ran[i].y);
		cout << (char)254;
	}
	gotoXY(dotcuoicu.x, dotcuoicu.y);
	cout << " ";
}

toado dichuyen(int huong)// Ham di chuyen
{
	toado dotcuoicu = ran[soDot - 1];
	for (int i = soDot - 1; i >= 1; i--)
	{
		ran[i] = ran[i - 1];
	}
	switch (huong)
	{
	case LEN:
		ran[0].y--;
		break;
	case XUONG:
		ran[0].y++;
		break;
	case TRAI:
		ran[0].x--;
		break;
	case PHAI:
		ran[0].x++;
		break;
	}
	return dotcuoicu;
}

void BatSuKien(int &huong) //Ham bat su kien
{
	int key = inputKey();
	if (key == 'w' || key == 'W' )
	{
		huong = LEN;
	}
	if (key == 's' || key == 'S')
	{
		huong = XUONG;
	}
	if (key == 'a' || key == 'A')
	{
		huong = TRAI;
	}
	if (key == 'd' || key == 'D')
	{
		huong = PHAI;
	}
}

void vetuong() //Ham ve tuong
{
	for (int x = TUONG_TRAI; x <= TUONG_PHAI; x++)
	{
		gotoXY(x, TUONG_TREN);
		cout << (char)220;
		gotoXY(x, TUONG_DUOI);
		cout << (char)220;
	}
	for (int y = TUONG_TREN + 1; y <= TUONG_DUOI; y++)
	{
		gotoXY(TUONG_TRAI, y);
		cout << (char)219;
		gotoXY(TUONG_PHAI, y);
		cout << (char)219;
	}
}

bool kiemtrathua() //Ham kiem tra thua
{
	if (ran[0].y == TUONG_TREN || ran[0].y == TUONG_DUOI)
		return 1;
	if (ran[0].x == TUONG_TRAI || ran[0].x == TUONG_PHAI)
		return 1;
}

void xulythua() //Ham xu ly thua
{
	if (kiemtrathua() == true)
	{
		Sleep(1000);//sleep 1s
		clrscr;
		cout << "BAN DA THUA CUOC";
	}
}

toado hienthimoi() //Ham hien thi moi
{
	srand(time(NULL));
	int x = TUONG_TRAI + 1 + rand() % (TUONG_PHAI - TUONG_TRAI - 2); //Random toa do x cua moi
	int y = TUONG_TREN + 1 + rand() % (TUONG_DUOI - TUONG_TREN - 2); //Random toa do y cua moi
	gotoXY(x, y);
	cout << (char)254;
	return toado{ x,y };
}

bool kiemtraanmoi(toado moi) //Ham kiem tra an moi
{
	if (ran[0].x == moi.x && ran[0].y == moi.y)
		return true;
	else return false;
}

void themDot() //Ham them dot ran
{
	ran[soDot] = ran[soDot - 1];
	soDot++;
}
